<?php

require 'GumballMachine.php';use PHPUnit\Framework\TestCase;

class GumballMachineTest extends TestCase
{
    public $gumballMachineInstance;

//    public function setUp() : void
//    {
//    }

    public function testIfWheelWorks()
    {
        $this->gumballMachineInstance = new GumballMachine();
        $this->gumballMachineInstance->setGumballs(100);
        $this->gumballMachineInstance->turnWheel();
        $this->assertEquals(99, $this->gumballMachineInstance->getGumballs());
    }

}